package com.mycompany.mavenproject1;

/**
 * Esta interface permite integrar o calculo do IRS e o seu desconto no salário
 * de um atleta que possui componente fixa no salário.
 *
 * @author 1181011 & 1181061
 */
public interface IRS {

    double TAXA_IRS = 10 / (double) 100;

    double calculoIRS();

}
