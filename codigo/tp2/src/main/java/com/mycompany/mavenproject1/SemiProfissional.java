package com.mycompany.mavenproject1;

/**
 * Representa um atleta SemiProfissional através do seu nome, NIC, género,
 * atividade, FCR, prémios, antiguidade e parcela fixa.
 *
 * @author 1181011 & 1181061
 */
public class SemiProfissional extends NaoProfissional implements IRS {

    /**
     * O parcela fixa a pagar ao atleta.
     */
    private double parcFixa;
    /**
     * O parcela fixa por omissão a pagar ao atleta.
     */
    private static double PFIXA_OMISSAO = 0;

    /**
     * O Número de atletas Semi Profissionais instânciados.
     */
    private static int nSemiProfissional = 0;

    /**
     * Contrói uma instância de SemiProfissional atribuindo o nome, o NIC, o
     * género, a atividade, o FCR, o IT, a parcela fixa, o valor de prémios e a
     * antiguidade introduzidos.
     *
     *
     * @param nome nome do atleta.
     * @param NIC NIC do atleta.
     * @param genero genero do atleta.
     * @param idade idade do atleta.
     * @param atividade atividade do atelta.
     * @param FCR frequência cardiaca em repouso do atleta.
     * @param IT intensidade de treino do atleta.
     * @param premios premios arrecadados pelo atleta.
     * @param antiguidade número de anos que o atleta tem no clube.
     */
    public SemiProfissional(String nome, int NIC, String genero, int idade, String atividade, double FCR, double IT, double premios, int antiguidade, double parcFixa) {
        super(nome, NIC, genero, idade, atividade, FCR, IT, premios, antiguidade);
        if (validaParcFixa(parcFixa)) {
            this.parcFixa = parcFixa;
            nSemiProfissional++;
        }
    }

    /**
     * Inicializa o atleta com o nome, NIC, género, atividade, FCR, prémios e
     * antiguidade por omissão.
     */
    public SemiProfissional() {
        super();
        parcFixa = PFIXA_OMISSAO;
        nSemiProfissional++;
    }

    /**
     * @return número de atletas semi profisionais instânciados.
     */
    public static int getnSemiProfissional() {
        return nSemiProfissional;
    }

    /**
     * @return valor da parcela fixa a ser paga.
     */
    public double getParcFixa() {
        return parcFixa;
    }

    /**
     * @param parcFixa novo valor da parcela fixa a ser paga.
     */
    public void setParcFixa(double parcFixa) {
        this.parcFixa = parcFixa;
    }

    /**
     * Devolve a descrição textual do atleta amador.
     *
     * @return caraterísticas de um atleta amador.
     */
    @Override
    public String toString() {
        return String.format(super.toString()
                + "PARCELA FIXA: %.2f€\n", parcFixa
        );
    }

    /**
     * Valida se o valor da parcela fixa é superior a 0 visto que é impossivel
     * esse valor ser inferior a 0.
     *
     * @param parcFixa parcela fixa a pagar ao atleta.
     * @return true ou false dependendo se o valor introduzido é válido ou não.
     */
    public boolean validaParcFixa(double parcFixa) {
        if (parcFixa > 0) {
            return true;
        }
        System.out.println("Parcela Fixa - Inválida.");
        return false;
    }

    /**
     * Calcula o valor do salário do atleta semi profissional.
     *
     * @return salário do atleta profissional.
     */
    @Override
    public double calculoSalario() {
        return parcFixa + getPercAntiguidade() / 100 * parcFixa - calculoIRS();
    }

    /**
     * Calcula o valor a pagar para IRS do atleta profissional.
     *
     * @return IRS do atleta profissional.
     */
    @Override
    public double calculoIRS() {
        return this.parcFixa * TAXA_IRS;
    }
}
