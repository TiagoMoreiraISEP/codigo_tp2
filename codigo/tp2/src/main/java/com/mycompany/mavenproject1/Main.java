package com.mycompany.mavenproject1;

import java.util.ArrayList;
import java.util.List;

public class Main {

    static double itGordura = 0.6;
    static double itCardio = 0.75;

    public static void main(String[] args) {
        Profissional p1 = new Profissional("Rui Alves", 12345678, "Masculino", 18, "Corrida", 60, itCardio, 300, 600);
        Profissional p2 = new Profissional("Frederico Pires", 98765432, "Masculino", 18, "Caminhada", 60, itGordura, 300, 180);
        Profissional p3 = new Profissional("Diogo Vale", 76846285, "Masculino", 20, "Natação", 75, itGordura, 450, 240);

        SemiProfissional sp1 = new SemiProfissional("João Silva", 65439861, "Masculino", 18, "Natação", 65, itGordura, 300, 7, 100);
        SemiProfissional sp2 = new SemiProfissional("Rita Santos", 53657684, "Feminino", 14, "Corrida", 76, itCardio, 630, 15, 142);
        SemiProfissional sp3 = new SemiProfissional("Inês Costa", 75647685, "Feminino", 25, "Ciclismo", 50, itGordura, 60, 2, 85);

        Amador a1 = new Amador("Pedro Silva", 85128553, "Masculino", 54, "Corrida", 76, itCardio, 674, 27);
        Amador a2 = new Amador("Beatriz Carvalho", 96755345, "Feminino", 32, "Caminhada", 54, itCardio, 200, 7);
        Amador a3 = new Amador("Ana Diogo", 65738657, "Feminino", 19, "Ciclismo", 67, itGordura, 170, 2);

        List<Atleta> contentorAtletas = new ArrayList<>();
        contentorAtletas.add(p1);
        contentorAtletas.add(p2);
        contentorAtletas.add(p3);
        contentorAtletas.add(sp1);
        contentorAtletas.add(sp2);
        contentorAtletas.add(sp3);
        contentorAtletas.add(a1);
        contentorAtletas.add(a2);
        contentorAtletas.add(a3);

        Data d1 = new Data(2015, 6, 25);
        ClubeDesportivo clube1 = new ClubeDesportivo("Clube do Fundão", d1, contentorAtletas);

        System.out.println("###Funcionalidade 1###");
        System.out.println("Nome do clube: " + clube1.getNomeDoClube());
        //Funcionalidade 2
        Amador a4 = new Amador("Nuno Taveira", 75478635, "Masculino", 27, "Natação", 60, itCardio, 87, 17);
        clube1.addAtleta(a4);
        System.out.println("###Funcionalidade 3###");
        listar(clube1.getAtletasPorNome());
        System.out.println("###Funcionalidade 4###");
        listar(clube1.getAtletasPorPremiosInverso());
        System.out.println("###Funcionalidade 5###");
        System.out.println(clube1.getTotalIRS());
        System.out.println("###Funcionalidade 6###");
        listar(clube1.getAtletaPorCatModNome());

    }

    public static void listar(List contentor) {
        for (Object o : contentor) {
            System.out.println(o);
        }
    }
}
