package com.mycompany.mavenproject1;

/**
 * Representa um atleta amador através do seu nome, NIC, género, atividade, FCR,
 * prémios e antiguidade.
 *
 * @author 1181011 & 1181061
 */
public class Amador extends NaoProfissional {

    /**
     * O valor mínimo a pagar ao atleta.
     */
    private static double minimoAReceber = 5;
    /**
     * Percentagem de prémios a pagar ao atleta.
     */
    private static double percentagemValorMensal = 7;
    /**
     * Númerero de atletas amadores instânciados.
     */
    private static int nAmadores = 0;

    /**
     * Contrói uma instância de Amador atribuindo o nome, o NIC, o género, a
     * atividade, o FCR, o IT, a parcela fixa e o valor de prémios introduzidos.
     *
     *
     * @param nome nome do atleta.
     * @param NIC NIC do atleta.
     * @param genero genero do atleta.
     * @param idade idade do atleta.
     * @param atividade atividade do atelta.
     * @param FCR frequência cardiaca em repouso do atleta.
     * @param IT intensidade de treino do atleta.
     * @param premios premios arrecadados pelo atleta.
     * @param antiguidade número de anos que o atleta tem no clube.
     */
    public Amador(String nome, int NIC, String genero, int idade, String atividade, double FCR, double IT, double premios, int antiguidade) {
        super(nome, NIC, genero, idade, atividade, FCR, IT, premios, antiguidade);
        nAmadores++;
    }

    /**
     * Constrói uma instância de Amador atribuindo o nome, o NIC, o género, a
     * atividade, o FCR, o IT,o valor de prémios e a antiguidade por omissão.
     */
    public Amador() {
        super();
        nAmadores++;
    }

    /**
     * @return número de atletas amadores instânciados.
     */
    public static int getnAmadores() {
        return nAmadores;
    }

    /**
     * @return valor mínimo a pagar ao atleta amador mensalmente.
     */
    public static double getMinimoAReceber() {
        return minimoAReceber;
    }

    /**
     * @param minimoAReceber novo valor mínimo a pagar ao atleta amador
     * mensalmente
     */
    public static void setMinimoAReceber(double minimoAReceber) {
        Amador.minimoAReceber = minimoAReceber;
    }

    /**
     * @return percentagem do valor dos prémios a pagar ao atleta amador
     * mensalmente.
     */
    public static double getPercentagemValorMensal() {
        return percentagemValorMensal;
    }

    /**
     * @param percentagemValorMensal nova percentagem do valor dos prémios a
     * pagar ao atleta amador mensalmente.
     *
     *
     */
    public static void setPercentagemValorMensal(double percentagemValorMensal) {
        Amador.percentagemValorMensal = percentagemValorMensal;
    }

    /**
     * Devolve a descrição textual do atleta amador.
     *
     * @return caraterísticas de um atleta amador.
     */
    @Override
    public String toString() {
        return super.toString();
    }

    /**
     * Calcula o valor do salário do atleta profissional.
     *
     * @return salário do atleta profissional.
     */
    @Override
    public double calculoSalario() {
        double salario = (getPercAntiguidade() / 100) * getPremios() + (percentagemValorMensal / 100) * getPremios();
        if (salario < minimoAReceber) {
            return minimoAReceber;
        }
        return salario;
    }
}
