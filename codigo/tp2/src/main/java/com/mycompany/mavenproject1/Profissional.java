package com.mycompany.mavenproject1;

/**
 * Representa um atleta profissional através do seu nome, NIC, género,
 * atividade, FCR, prémios e percentagem fixa.
 *
 * @author 1181011 & 1181061
 */
public class Profissional extends Atleta implements IRS {

    /**
     * O parcela fixa a pagar ao atleta.
     */
    private double parcFixa;
    /**
     * O parcela fixa por omissão a pagar ao atleta.
     */
    private static final double PARC_FIXA_OMISSAO = 0;
    /**
     * O parcela variável a pagar ao atleta.
     */
    public static double percVariavel = 20;
    /**
     * Número de atletas profissionais instânciados.
     */
    private static int nProfissionais = 0;

    /**
     * Constrói uma instância de Profissionais atribuindo o nome, o NIC, o
     * género, a atividade, o FCR, o IT, a parcela fixa e o valor de prémios por
     * omissão.
     */
    public Profissional() {
        super();
        parcFixa = PARC_FIXA_OMISSAO;
        nProfissionais++;
    }

    /**
     * Contrói uma instância de Profissionais atribuindo o nome, o NIC, o
     * género, a atividade, o FCR, o IT, a parcela fixa e o valor de prémios
     * introduzidos.
     *
     *
     * @param nome nome do atleta.
     * @param NIC NIC do atleta.
     * @param genero genero do atleta.
     * @param idade idade do atleta.
     * @param atividade atividade do atelta.
     * @param FCR frequência cardiaca em repouso do atleta.
     * @param IT intensidade de treino do atleta.
     * @param parcFixa valor fixo a pagar ao atleta.
     * @param premios premios arrecadados pelo atleta.
     */
    public Profissional(String nome, int NIC, String genero, int idade, String atividade, double FCR, double IT, double parcFixa, double premios) {
        super(nome, NIC, genero, idade, atividade, FCR, IT, premios);
        if (validaParcFixa(parcFixa)) {
            this.parcFixa = parcFixa;
            nProfissionais++;
        }
    }

    /**
     * @return número de atletas profissionais instânciados.
     */
    public static int getnProfissionais() {
        return nProfissionais;
    }

    /**
     * @return parcela fixa a pagar ao atleta.
     */
    public double getParcFixa() {
        return parcFixa;
    }

    /**
     * @param parcFixa nova parcela fixa a pagar ao atleta.
     */
    public void setParcFixa(double parcFixa) {
        this.parcFixa = parcFixa;
    }

    /**
     * @return percentagem de prémios a pagar ao atleta.
     */
    public static double getPercVariavel() {
        return percVariavel;
    }

    /**
     * @param aPercVariavel nova percentagem de prémios a pagar ao atleta.
     */
    public static void setPercVariavel(double aPercVariavel) {
        percVariavel = aPercVariavel;
    }

    /**
     * Valida se o valor da parcela fixa é superior a 0 visto que é impossivel
     * essa parcela ser inferior a 0.
     *
     * @param parcFixa parcela fixa a ser paga ao atleta profissiomal.
     * @return true ou false dependendo se o valor introduzido é válido ou não.
     */
    public boolean validaParcFixa(double parcFixa) {
        if (parcFixa > 0) {
            return true;
        } else {
            System.out.println("Parcela Fixa - Inválida.");
            return false;
        }
    }

    /**
     * Devolve a descrição textual do atleta profissional.
     *
     * @return caraterísticas de um atleta profissional.
     */
    @Override
    public String toString() {
        return String.format(super.toString()
                + "PARCELA FIXA: %.2f€ \n"
                + "PERCENTAGEM VARIÁVEL: %.2f%% \n", parcFixa, percVariavel
        );
    }

    /**
     * Calcula o valor do salário do atleta profissional.
     *
     * @return salário do atleta profissional.
     */
    @Override
    public double calculoSalario() {
        return ((percVariavel / 100) * getPremios()) + parcFixa - calculoIRS();
    }

    /**
     * Calcula o valor a pagar para IRS do atleta profissional.
     *
     * @return IRS do atleta profissional.
     */
    @Override
    public double calculoIRS() {
        return this.parcFixa * TAXA_IRS;
    }

}
