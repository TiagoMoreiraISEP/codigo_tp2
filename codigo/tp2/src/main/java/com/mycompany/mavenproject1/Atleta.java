package com.mycompany.mavenproject1;

/**
 * Esta classe permite a construção de uma hierarquia de classes para
 * representarem diferentes tipos de atletas. Especifica membros comuns a todas
 * as classes da hierarquia.
 *
 * @author 1181011 & 1181061
 */
public abstract class Atleta implements Comparable<Atleta> {

    /**
     * O nome do atleta.
     */
    private String nome;
    /**
     * O nome por omissão do atleta.
     */
    private static final String NOME_OMISSAO = "SEM NOME";
    /**
     * O NIC do atleta.
     */
    private int NIC;
    /**
     * O NIC por omissão do atleta.
     */
    private static final int NIC_OMISSAO = 0;
    /**
     * O género do atleta.
     */
    private String genero;
    /**
     * O género por omissão do atleta.
     */
    private static final String GENERO_OMISSAO = "Outro";
    /**
     * A idade do atleta.
     */
    private int idade;
    /**
     * A idade por omissão do atleta.
     */
    private static final int IDADE_OMISSAO = 0;
    /**
     * A atividade do atleta.
     */
    private String atividade;
    /**
     * A atividade por omissão do atleta.
     */
    private static final String ATIVIDADE_OMISSAO = "Outro";
    /**
     * A Frequência Cardiaca em Repouso do atleta.
     */
    private double FCR;
    /**
     * A Frequência Cardiaca em Repouso por omissão do atleta.
     */
    private static final double FCR_OMISSAO = 0;
    /**
     * A Itensidade de Treino do atleta caso este seja com o intuito perder
     * gordura.
     */
    private static final double IT_GORDURA = 0.6;
    /**
     * A Itensidade de Treino do atleta caso este seja com o intuito de cárdio.
     */
    private static final double IT_CARDIO = 0.75;
    /**
     * A Itensidade de Treino do atleta por omissão.
     */
    private static final double IT_OMISSAO = 0;
    /**
     * A Itensidade de Treino do atleta.
     */
    private double IT;
    /**
     * O valor em prémios arrecadado pelo atleta.
     */
    private double premios;
    /**
     * O valor em prémios arrecadado pelo atleta por omissão.
     */
    private static final double PREMIOS_OMISSAO = 0;

    /**
     * Valor não alteravél mostrado na tabela do cálculo da FCM.
     */
    private static final double VALOR1_FCM = 208.75;
    /**
     * Valor não alteravél mostrado na tabela do cálculo da FCM.
     */
    private static final double VALOR2_FCM = 189;
    /**
     * Valor não alteravél mostrado na tabela do cálculo da FCM.
     */
    private static final double VALOR3_FCM = 202;
    /**
     * Valor não alterável mostrado na tabela do cálculo da FCM.
     */
    private static final double VALOR4_FCM = 204;
    /**
     * Valor não alteravél mostrado na tabela do cálculo da FCM.
     */
    private static final double PERC1_FCM = 0.73;
    /**
     * Valor não alteravél mostrado na tabela do cálculo da FCM.
     */
    private static final double PERC2_FCM = 0.56;
    /**
     * Valor não alteravél mostrado na tabela do cálculo da FCM.
     */
    private static final double PERC3_FCM = 0.72;
    /**
     * Valor não alteravél mostrado na tabela do cálculo da FCM.
     */
    private static final double PERC4_FCM = 1.7;
    /**
     * Número de atletas existentes.
     */
    private static int nAtletas = 0;

    /**
     * Inicializa o atleta com o nome, NIC, género, atividade, FCR e prémios por
     * omissão.
     */
    public Atleta() {
        nome = NOME_OMISSAO;
        NIC = NIC_OMISSAO;
        genero = GENERO_OMISSAO;
        atividade = ATIVIDADE_OMISSAO;
        FCR = FCR_OMISSAO;
        premios = PREMIOS_OMISSAO;
        idade = IDADE_OMISSAO;
        IT = IT_OMISSAO;
        nAtletas++;
    }

    /**
     * Inicializa o atleta com o nome, NIC, género, atividade, FCR e prémios
     * introduzidos.
     *
     *
     * @param nome nome do atleta.
     * @param NIC NIC do atleta.
     * @param genero genero do atleta.
     * @param idade idade do atleta.
     * @param atividade atividade do atelta.
     * @param FCR frequência cardiaca em repouso do atleta.
     * @param IT intensidade de treino do atleta.
     * @param premios premios arrecadados pelo atleta.
     */
    public Atleta(String nome, int NIC, String genero, int idade, String atividade, double FCR, double IT, double premios) {
        if (validaFCR(FCR) && validaAtividade(atividade) && validaGenero(genero) && validaIT(IT) && validaIdade(idade)) {
            this.nome = nome;
            this.NIC = NIC;
            this.genero = genero;
            this.idade = idade;
            this.atividade = atividade;
            this.FCR = FCR;
            this.IT = IT;
            this.premios = premios;
            nAtletas++;
        }
    }

    /**
     * @return nome do atleta.
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome novo nome do atleta.
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return NIC do atleta.
     */
    public int getNIC() {
        return NIC;
    }

    /**
     * @param NIC novo NIC do atleta.
     */
    public void setNIC(int NIC) {
        this.NIC = NIC;
    }

    /**
     * @return genero do atleta.
     */
    public String getGenero() {
        return genero;
    }

    /**
     * @param genero novo genero do atleta.
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * @return idade do atleta.
     */
    public int getIdade() {
        return idade;
    }

    /**
     * @param idade nova idade do atleta.
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }

    /**
     * @return atividade do atleta.
     */
    public String getAtividade() {
        return atividade;
    }

    /**
     * @param atividade nova atividade do atleta.
     */
    public void setAtividade(String atividade) {
        this.atividade = atividade;
    }

    /**
     * @return Frequência Cardiaca em Repouso do atleta.
     */
    public double getFCR() {
        return FCR;
    }

    /**
     * @param FCR nova Frequência Cardiaca em Repouso do atleta
     */
    public void setFCR(double FCR) {
        this.FCR = FCR;
    }

    /**
     * @return Intensidade de Treino do atleta.
     */
    public double getIT() {
        return IT;
    }

    /**
     * @param IT nova Intensidade de Treino do atleta.
     */
    public void setIT(double IT) {
        this.IT = IT;
    }

    /**
     * @return premios arrecadados pelo atleta.
     */
    public double getPremios() {
        return premios;
    }

    /**
     * @param premios novos premios arrecadados pelo atleta.
     */
    public void setPremios(double premios) {
        this.premios = premios;
    }

    /**
     * @return número de atletas criados.
     */
    public static int getnAtletas() {
        return nAtletas;
    }

    /**
     * Valida se o valor da Frequência Cardiaca em Repouso é superior a 0 visto
     * que é impossivel um atleta ter Frequência Cardiaca inferior a 0.
     *
     * @param fcr Frequência cardiaca em repouso.
     * @return true ou false dependendo se o valor introduzido é válido ou não.
     */
    public boolean validaFCR(double fcr) {
        if (fcr > 0) {
            return true;
        }
        System.out.println("Frequência Cardiaca em Repouso - Inválida.");
        return false;
    }

    /**
     * Valida se o valor da idade é superior a 0 visto que é impossivel um
     * atleta ter idade 0 ou inferior.
     *
     * @param idade idade do atleta.
     * @return true ou false dependendo se o valor introduzido é válido ou não.
     */
    public boolean validaIdade(int idade) {
        if (idade > 0) {
            return true;
        }
        System.out.println("Idade - Inválida.");
        return false;
    }

    /**
     * Valida se a atividade introduzida é uma das atividades válidas ou se é
     * uma atividade desconhecida ao sistema.
     *
     * @param atividade idade do atleta.
     * @return true ou false dependendo se a atividade introduzida é válida ou
     * não.
     */
    public boolean validaAtividade(String atividade) {
        if (atividade.equalsIgnoreCase("Caminhada") || atividade.equalsIgnoreCase("Corrida")
                || atividade.equalsIgnoreCase("Ciclismo") || atividade.equalsIgnoreCase("Natação")) {
            return true;
        }
        System.out.println("Atividade - Inválida.");
        return false;
    }

    /**
     * Valida se o género introduzida é válido. Para estes fins foi considerado
     * como "Masculino" e "Feminino" sendo os únicos géneros existentes.
     *
     * @param genero género do atleta.
     * @return true ou false dependendo se o género introduzido é válido ou não.
     */
    public boolean validaGenero(String genero) {
        if (genero.equalsIgnoreCase("Masculino") || genero.equalsIgnoreCase("Feminino")) {
            return true;
        }
        System.out.println("Género - Inválido.");
        return false;
    }

    /**
     * Valida se o valor da Intensidade de Treino é um dos dois valores
     * conhecidos do sistema: a intensidade de treino para perder gordura ou a
     * intensidade de treino para cárdio.
     *
     * @param IT Intensidade de Treino do atleta.
     * @return true ou false dependendo se a intensidade introduzida é válida ou
     * não.
     */
    public boolean validaIT(double IT) {
        if (IT == IT_GORDURA || IT == IT_CARDIO) {
            return true;
        }
        System.out.println("Intensidade de Treino - Inválido");
        return false;
    }

    /**
     * Devolve a descrição textual do atleta.
     *
     * @return caraterísticas de um atleta.
     */
    @Override
    public String toString() {
        return String.format("NOME: %s \n"
                + "NIC: %d \n"
                + "GENERO: %s \n"
                + "ATIVIDADE: %s \n"
                + "FREQ. CARDIACA EM REPOUSO: %.2f \n"
                + "INTENSIDADE DE TREINO: %.2f \n"
                + "PRÉMIOS: %.2f€ \n", this.getNome(), this.getNIC(), this.getGenero(), this.getAtividade(), this.getFCR(), this.getIT(), this.getPremios());
    }

    @Override
    public int compareTo(Atleta outroAtleta) {
        return nome.compareTo(outroAtleta.nome);
    }

    /**
     * Cálcula o valor da Frequência Cardiaca em Movimento com base nos valores
     * da tabela disponibilizada.
     *
     * @return Frequência cardiaca em movimento do atleta.
     */
    public double calculoFCM() {
        if (this.getAtividade().equalsIgnoreCase("Caminhada") || this.getAtividade().equalsIgnoreCase("Corrida")) {
            return VALOR1_FCM - (PERC1_FCM * this.getIdade());
        } else if (this.getAtividade().equalsIgnoreCase("Ciclismo") && this.getGenero().equalsIgnoreCase("Feminino")) {
            return VALOR2_FCM - (PERC2_FCM * this.getIdade());
        } else if (this.getAtividade().equalsIgnoreCase("Caminhada") && this.getGenero().equalsIgnoreCase("Masculino")) {
            return VALOR3_FCM - (PERC3_FCM * this.getIdade());
        } else if (this.getAtividade().equalsIgnoreCase("Natação")) {
            return VALOR4_FCM - (PERC4_FCM * this.getIdade());
        }
        return 0;
    }

    /**
     * Cálcula o valor da Frequência Cardiaca de Trabalho com base na fórmula
     * disponibilizada.
     *
     * @return Frequência Cardiaca de Trabalho do atleta.
     */
    public double calculoFCT() {
        return this.getFCR() + (this.getIT() * (this.calculoFCM() - this.getFCR()));
    }

    /**
     * Permite o cálculo do salário a pagar a cada atleta através do
     * polimorfismo.
     *
     * @return salário do atleta.
     */
    public abstract double calculoSalario();
}
