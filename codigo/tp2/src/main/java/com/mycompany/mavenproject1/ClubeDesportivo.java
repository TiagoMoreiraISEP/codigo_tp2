package com.mycompany.mavenproject1;

import java.util.*;

/**
 * Representa um clube desportivo através do seu nome, data de fundação e
 * atletas que o integram.
 *
 * @author 1181011 & 1181061
 */
public class ClubeDesportivo {

    /**
     * Nome do clube.
     */
    private String nomeDoClube;
    /**
     * Nome do clube por omissão.
     */
    private static final String NOME_OMISSAO = "Sem Nome";
    /**
     * Data de fundação do clube.
     */
    private Data dataDeFundacao;
    /**
     * Lista de atletas do clube.
     */
    private List<Atleta> atletas;

    /**
     * Constrói uma instância de Clube Desportivo atribuindo o nome, a data de
     * fundação e a lista de atletas por omissão.
     */
    public ClubeDesportivo() {
        this.nomeDoClube = NOME_OMISSAO;
        this.dataDeFundacao = new Data();
        this.atletas = new ArrayList<>();
    }

    /**
     * Constrói uma instância de Clube Desportivo atribuindo o nome, a data de
     * fundação e a lista de atletas introduzidos.
     *
     *
     * @param nome nome do clube.
     * @param dataDeFundacao data de fundação do clube.
     * @param atletas lista de atletas do clube.
     */
    public ClubeDesportivo(String nome, Data dataDeFundacao, List<Atleta> atletas) {
        this.nomeDoClube = nome;
        this.dataDeFundacao = new Data(dataDeFundacao);
        this.atletas = new ArrayList<>(atletas);
    }

    /**
     * Constrói uma instância de Clube Desportivo atribuindo o nome e a data de
     * fundação.
     *
     * @param nome nome do clube.
     * @param dataDeFundacao data de fundação do clube.
     */
    public ClubeDesportivo(String nome, Data dataDeFundacao) {
        this.nomeDoClube = nome;
        this.dataDeFundacao = new Data(dataDeFundacao);
        this.atletas = new ArrayList<>();
    }

    /**
     * @return nomeDoClube nome do clube.
     */
    public String getNomeDoClube() {
        return nomeDoClube;
    }

    /**
     * @param nomeDoClube novo nome do clube.
     */
    public void setNomeDoClube(String nomeDoClube) {
        this.nomeDoClube = nomeDoClube;
    }

    /**
     * @return dataDeFundacao do clube.
     */
    public Data getDataDeFundacao() {
        return dataDeFundacao;
    }

    /**
     * @param dataDeFundacao nova dataDeFundacao do clube.
     */
    public void setDataDeFundacao(Data dataDeFundacao) {
        this.dataDeFundacao = dataDeFundacao;
    }

    /**
     * @return lista com os atletas do clube.
     */
    public List<Atleta> getAtletas() {
        return atletas;
    }

    /**
     * Ordena os atletas por nome, utilizando o Comparable implementado na
     * classe Atleta
     *
     * @return atletas ordenado por nome
     */
    public List<Atleta> getAtletasPorNome() {
        List<Atleta> atletasSort = new ArrayList<>(atletas);
        Collections.sort(atletasSort);
        return atletasSort;
    }

    /**
     * Ordena os atletas por prémios de ordem decrescente, utilizando uma classe
     * interna Comparator.
     *
     *
     * @return atletas ordenado por prémios em ordem decrescente.
     */
    public List<Atleta> getAtletasPorPremiosInverso() {
        Comparator c1 = new Comparator() {
            @Override
            public int compare(Object obj1, Object obj2) {
                Atleta a1 = (Atleta) obj1;
                Atleta a2 = (Atleta) obj2;
                return (int) (a2.getPremios() - a1.getPremios());
            }
        };

        List<Atleta> atletasSort = new ArrayList<>(atletas);
        Collections.sort(atletasSort, c1);
        return atletasSort;
    }

    /**
     * Ordena os atletas por Categoria, Modalidade e nome, utilizando uma classe
     * interna Comparator.
     *
     *
     * @return atletas ordenado por Categoria, Modalidade e nome.
     */
    public List<Atleta> getAtletaPorCatModNome() {
        Comparator c1 = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Atleta a1 = (Atleta) o1;
                Atleta a2 = (Atleta) o2;
                if (a1.getClass().toString().compareTo(a2.getClass().toString()) == 0) {
                    if (a1.getAtividade().compareToIgnoreCase(a2.getAtividade()) == 0) {
                        return a1.getNome().compareToIgnoreCase(a2.getNome());
                    } else {
                        return a1.getAtividade().compareToIgnoreCase(a2.getAtividade());
                    }
                } else {
                    return a1.getClass().toString().compareToIgnoreCase(a2.getClass().toString());
                }
            }
        };

        List<Atleta> atletasSort = new ArrayList<>(atletas);
        Collections.sort(atletasSort, c1);
        return atletasSort;
    }

    /**
     * Permite adicionar um novo atleta à lista de atletas do clube, caso este
     * ainda não esteja no mesmo.
     *
     *
     * @param a novo atleta a adicionar.
     * @return true ou false dependendo se o atleta já existia no clube ou não.
     */
    public boolean addAtleta(Atleta a) {
        return atletas.contains(a) ? false : atletas.add(a);
    }

    /**
     * Permite comparar dois clubes desportivos. *
     *
     * @param outroObjeto outro clube desportivo.
     * @return true ou false dependendo os clubes são iguais ou não.
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        ClubeDesportivo outroClube = (ClubeDesportivo) outroObjeto;
        return (this.nomeDoClube.equalsIgnoreCase(outroClube.getNomeDoClube())
                && this.getDataDeFundacao().equals(outroClube.getDataDeFundacao())
                && this.getAtletas().equals(outroClube.getAtletas()));
    }

    /**
     * Permite obter o valor total descontado para IRS dos atletas de um clube
     * desportivo.
     *
     * @return valor total descontado para IRS dos atletas do clube.
     */
    public double getTotalIRS() {
        double totalIRS = 0;
        for (Atleta a : atletas) {
            if (a instanceof IRS) {
                totalIRS += ((IRS) a).calculoIRS();
            }
        }
        return totalIRS;
    }
}
