package com.mycompany.mavenproject1;

/**
 * Esta classe permite a construção de uma hierarquia de classes para
 * representarem diferentes tipos de atletas não profissionais. Especifica
 * membros comuns a todos os atletas não profissionais.
 *
 * @author 1181011 & 1181061
 */
public abstract class NaoProfissional extends Atleta {

    /**
     * Antiguidade do atleta.
     */
    private int antiguidade;
    /**
     * Antiguidade do atleta por omissão.
     */
    private static int ANTIGUIDADE_OMISSAO = 0;

    /**
     * Valor utilizado no cálculo da percentagem mensal a pagar tendo como base
     * a tabela 2 do enunciado.
     */
    private static double percentagem1 = 2;
    /**
     * Valor utilizado no cálculo da percentagem mensal a pagar tendo como base
     * a tabela 2 do enunciado.
     */
    private static double percentagem2 = 8;
    /**
     * Valor utilizado no cálculo da percentagem mensal a pagar tendo como base
     * a tabela 2 do enunciado.
     */
    private static double percentagem3 = 20;

    /**
     * Número de atletas não profissionais instânciados.
     */
    private static int nNaoProfissionais = 0;

    /**
     * Contrói uma instância de NaoProfissional atribuindo o nome, o NIC, o
     * género, a atividade, o FCR, o IT, a parcela fixa e o valor de prémios
     * introduzidos.
     *
     *
     * @param nome nome do atleta.
     * @param NIC NIC do atleta.
     * @param genero genero do atleta.
     * @param idade idade do atleta.
     * @param atividade atividade do atelta.
     * @param FCR frequência cardiaca em repouso do atleta.
     * @param IT intensidade de treino do atleta.
     * @param premios premios arrecadados pelo atleta.
     * @param antiguidade número de anos que o atleta tem no clube.
     */
    public NaoProfissional(String nome, int NIC, String genero, int idade, String atividade, double FCR, double IT, double premios, int antiguidade) {
        super(nome, NIC, genero, idade, atividade, FCR, IT, premios);
        if (validaAntiguidade(antiguidade)) {
            this.antiguidade = antiguidade;
            nNaoProfissionais++;
        }
    }

    /**
     * Constrói uma instância de NãoProfissional atribuindo o nome, o NIC, o
     * género, a atividade, o FCR, o IT,o valor de prémios e a antiguidade por
     * omissão.
     */
    public NaoProfissional() {
        super();
        this.antiguidade = ANTIGUIDADE_OMISSAO;
        nNaoProfissionais++;
    }

    /**
     * @return número de atletas não profisionais instânciados.
     */
    public static int getnNaoProfissionais() {
        return nNaoProfissionais;
    }

    /**
     * @return número de anos que o atleta tem no clube.
     */
    public int getAntiguidade() {
        return antiguidade;
    }

    /**
     * @param antiguidade novo número de anos que o atleta tem no clube
     */
    public void setAntiguidade(int antiguidade) {
        this.antiguidade = antiguidade;
    }

    /**
     * @return Valor utilizado no cálculo da percentagem mensal a pagar tendo
     * como base a tabela 2 do enunciado.
     */
    public static double getPercentagem1() {
        return percentagem1;
    }

    /**
     * @param percentagem1 novo valor utilizado no cálculo da percentagem mensal
     * a pagar tendo como base a tabela 2 do enunciado.
     */
    public static void setPercentagem1(double percentagem1) {
        NaoProfissional.percentagem1 = percentagem1;
    }

    /**
     * @return Valor utilizado no cálculo da percentagem mensal a pagar tendo
     * como base a tabela 2 do enunciado.
     */
    public static double getPercentagem2() {
        return percentagem2;
    }

    /**
     * @param percentagem2 novo valor utilizado no cálculo da percentagem mensal
     * a pagar tendo como base a tabela 2 do enunciado.
     */
    public static void setPercentagem2(double percentagem2) {
        NaoProfissional.percentagem2 = percentagem2;
    }

    /**
     * @return Valor utilizado no cálculo da percentagem mensal a pagar tendo
     * como base a tabela 2 do enunciado.
     */
    public static double getPercentagem3() {
        return percentagem3;
    }

    /**
     * @param percentagem3 novo valor utilizado no cálculo da percentagem mensal
     * a pagar tendo como base a tabela 2 do enunciado.
     */
    public static void setPercentagem3(double percentagem3) {
        NaoProfissional.percentagem3 = percentagem3;
    }

    /**
     * Valida se o valor da antiguidade é superior a 0 visto que é impossivel
     * esse valor ser inferior a 0.
     *
     * @param antiguidade número de anos que o atleta tem no clube.
     * @return true ou false dependendo se o valor introduzido é válido ou não.
     */
    public boolean validaAntiguidade(double antiguidade) {
        if (antiguidade > 0) {
            return true;
        }
        System.out.println("Antiguidade - Inválida.");
        return false;
    }

    /**
     * Devolve a descrição textual do atleta não profissional.
     *
     * @return caraterísticas de um atleta não profissional.
     */
    @Override
    public String toString() {
        return String.format(super.toString()
                + "ANTIGUDADE: %d anos \n", antiguidade
        );
    }

    /**
     * Tendo como base os valores da tabela 2 apresentada no enunciado, devolve
     * o valor da percentagem de prémios a ser paga com base na antiguidade do
     * atleta não profissional.
     *
     * @return 2, 8 ou 20 dependendo da antiguidade.
     */
    public double getPercAntiguidade() {
        if (antiguidade >= 5 && antiguidade <= 10) {
            return percentagem1;
        } else if (antiguidade > 10 && antiguidade <= 20) {
            return percentagem2;
        } else if (antiguidade > 20) {
            return percentagem3;
        }
        return 0;
    }

    /**
     * Permite o cálculo do salário a pagar a cada atleta através do
     * polimorfismo.
     *
     * @return salário do atleta.
     */
    @Override
    public abstract double calculoSalario();
}
