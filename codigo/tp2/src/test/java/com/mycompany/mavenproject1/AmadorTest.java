package com.mycompany.mavenproject1;

import org.junit.Test;

import static org.junit.Assert.*;

public class AmadorTest {

    /**
     * Cria uma instância da classe Amador e compara o resultado do método calculoSalario() com o resultado esperado
     */
    @Test
    public void calculoSalario() {
        Amador a1 = new Amador("Pedro Silva", 85128553, "Masculino", 54, "Corrida", 76, 0.75, 674, 2);
        double expResult = 47.18;
        double result = a1.calculoSalario();
        assertEquals(expResult, result, 0.01);

        a1.setAntiguidade(5);
        expResult = 60.66;
        result = a1.calculoSalario();
        assertEquals(expResult, result, 0.01);

        a1.setAntiguidade(7);
        expResult = 60.66;
        result = a1.calculoSalario();
        assertEquals(expResult, result, 0.01);

        a1.setAntiguidade(10);
        expResult = 60.66;
        result = a1.calculoSalario();
        assertEquals(expResult, result, 0.01);

        a1.setAntiguidade(15);
        expResult = 101.1;
        result = a1.calculoSalario();
        assertEquals(expResult, result, 0.01);

        a1.setAntiguidade(20);
        expResult = 101.1;
        result = a1.calculoSalario();
        assertEquals(expResult, result, 0.01);

        a1.setAntiguidade(25);
        expResult = 181.98;
        result = a1.calculoSalario();
        assertEquals(expResult, result, 0.01);
    }
}