package com.mycompany.mavenproject1;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProfissionalTest {

    /**
     * Cria uma instância da classe Profissional e compara o resultado do método calculoSalario() com o resultado esperado
     */
    @org.junit.Test
    public void calculoSalario() {
        Profissional p1 = new Profissional("Rui Alves", 12345678, "Masculino", 18, "Corrida", 60, 0.75, 300, 600);
        double expResult = 390;
        double result = p1.calculoSalario();
        assertEquals(expResult, result, 0.01);
    }
}