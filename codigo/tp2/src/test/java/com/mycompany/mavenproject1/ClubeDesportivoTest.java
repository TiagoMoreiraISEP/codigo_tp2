package com.mycompany.mavenproject1;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.mycompany.mavenproject1.Main.itCardio;
import static com.mycompany.mavenproject1.Main.itGordura;
import static org.junit.Assert.*;

/**
 * Testes aos métodos das opções getNomeDoClube, getAtletasPorNome e addAtleta.
 *
 * @author 1181011 & 1181061
 */
public class ClubeDesportivoTest {

    /**
     * Teste ao método getNomeDoClube.
     *
     */
    @Test
    public void getNomeDoClube() {
        ClubeDesportivo c1 = new ClubeDesportivo("Clube 1", new Data(2015, 1, 1));
        String expected = "Clube 1";
        String result = c1.getNomeDoClube();
        assertEquals(expected, result);
    }

    /**
     * Teste ao método getAtletasPorNome.
     *
     */
    @Test
    public void getAtletasPorNome() {
        Profissional a1 = new Profissional("Rui Alves", 12345678, "Masculino", 18, "Corrida", 60, itCardio, 300, 600);
        Amador a2 = new Amador("Beatriz Carvalho", 96755345, "Feminino", 32, "Caminhada", 54, itCardio, 200, 7);
        SemiProfissional a3 = new SemiProfissional("João Silva", 65439861, "Masculino", 18, "Natação", 65, itGordura, 300, 7, 100);
        List<Atleta> lstAtletas = new ArrayList<>();
        lstAtletas.add(a1);
        lstAtletas.add(a2);
        lstAtletas.add(a3);
        ClubeDesportivo c1 = new ClubeDesportivo("clube1", new Data(), lstAtletas);
        List<Atleta> lstExpected = new ArrayList<>();
        lstExpected.add(a2);
        lstExpected.add(a3);
        lstExpected.add(a1);
        assertArrayEquals(lstExpected.toArray(), c1.getAtletasPorNome().toArray());
    }

    /**
     * Teste ao método addAtleta.
     *
     */
    @Test
    public void addAtleta() {
        Profissional a1 = new Profissional("Rui Alves", 12345678, "Masculino", 18, "Corrida", 60, itCardio, 300, 600);
        Amador a2 = new Amador("Beatriz Carvalho", 96755345, "Feminino", 32, "Caminhada", 54, itCardio, 200, 7);
        SemiProfissional a3 = new SemiProfissional("João Silva", 65439861, "Masculino", 18, "Natação", 65, itGordura, 300, 7, 100);
        List<Atleta> lstAtletas = new ArrayList<>();
        lstAtletas.add(a1);
        lstAtletas.add(a2);
        lstAtletas.add(a3);
        ClubeDesportivo c1 = new ClubeDesportivo("clube1", new Data(), lstAtletas);

        Amador a4 = new Amador("Pedro Silva", 85128553, "Masculino", 54, "Corrida", 76, itCardio, 674, 27);
        boolean expected = true;
        assertEquals(expected, c1.addAtleta(a4));

        expected = false;
        assertEquals(expected, c1.addAtleta(a3));
    }
}
